<?php
if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
    include( dirname( __FILE__ ) . '/wp-config-local.php' );

    // Local Environment
    define('WP_ENV', 'local');
    define('WP_DEBUG', false);    
    
    // Table prefix Flyttad till lokal konfig fil
    // $table_prefix  = 'wp_'; 
    
    require( 'wp-config-local.php' );

} elseif ( file_exists( dirname( __FILE__ ) . '/wp-config-staging.php' ) ) {
	  include( dirname( __FILE__ ) . '/wp-config-staging.php' );

    // Playground Environment
    define('WP_ENV', 'playground');
    define('WP_DEBUG', false);
    define('WP_CACHE', true);
    define( 'AUTOMATIC_UPDATER_DISABLED', true ); 

} elseif ( file_exists( dirname( __FILE__ ) . '/wp-config-production.php' ) ) {
	  include( dirname( __FILE__ ) . '/wp-config-production.php' );

    // Production Environment
    define('WP_ENV', 'production');
    define('WP_DEBUG', false);
    define('WP_CACHE', true);
    define( 'AUTOMATIC_UPDATER_DISABLED', true ); 

    require( 'wp-config-production.php' );
} else {

    define('WP_ENV', 'local');
    define('WP_DEBUG', false);
    define('WP_CACHE', true);
    define( 'AUTOMATIC_UPDATER_DISABLED', true ); 

    define('WP_HOME', 'http://vagrant.lauraeus.se'); // no trailing slash
    define('WP_SITEURL', 'http://vagrant.lauraeus.se');  // no trailing slash
        
    #define('WP_ALLOW_MULTISITE', true);

    #define('MULTISITE', true);
    #define('SUBDOMAIN_INSTALL', true);
    #$base = '/';
    #define('DOMAIN_CURRENT_SITE', 'PRODSITEURL');
    #define('PATH_CURRENT_SITE', '/');
    #define('SITE_ID_CURRENT_SITE', 1);
    #define('BLOG_ID_CURRENT_SITE', 1);

    define('DB_NAME', 'wordpress');
    define('DB_USER', 'wordpress');
    define('DB_PASSWORD', 'wordpress-vagrant');
    define('DB_HOST', 'localhost');    
    $table_prefix  = 'wp_';
}

/** Teckenkodning f�r tabellerna i databasen. */
define('DB_CHARSET', 'utf8');

/** Kollationeringstyp f�r databasen. �ndra inte om du �r os�ker. */
define('DB_COLLATE', '');

define('AUTH_KEY',         '.uTLjWtMmz6?)K==|tM%x/.,r79,jgyx&<saf]2Q7}M;kX05+5aGM`O^o]cXup{<');
define('SECURE_AUTH_KEY',  'hGLG`?AIr_,@YeN)Ht)`+U~,)h*N,4`+MfXo4_0=U Z-8,a`=W ;@UbP{WahdT}4');
define('LOGGED_IN_KEY',    'gD+{&L7n(9S;O}oHcS_qd2Wu%pGUuQ`vAG0+Rv$C6f~Q/Z:,Mv}A ?tv.}{r1^}A');
define('NONCE_KEY',        'im~V |U%Q7q%zW{w~|q3Xc aj^NVD.DaEBB|l.tS:ML7(f({6!0`>]1[i:Gg)f2Y');
define('AUTH_SALT',        '[_R@^5}KnvkCum9>YxjzO4WncUd+^Q4LD,~g|M&NDd(>c6nM7q4r1.5RihKRa/P-');
define('SECURE_AUTH_SALT', 'l(YrfX`#x_/&gp)7J9uc6`F`t3MDKhzXRjEe2LI29wI~vw2]jy|YxVRE3i$G+<<|');
define('LOGGED_IN_SALT',   'c<VI^MfO+R5l8gO)-lC]zQt$U;4Ja=c$VUm#(OkKM/$Uk~CA,H5L$!Y;9~b+8 3l');
define('NONCE_SALT',       'bm1$!lEDucc@+M8.:!rUa5k9Zwho6_$5|AXT0`|yvy]=pq$AJ+;t?}:`r&$]|0BB');



define ('WPLANG', 'sv_SE');

define('DISALLOW_FILE_EDIT', true);


/** Absoluta s�kv�g till WordPress-katalogen. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Anger WordPress-v�rden och inkluderade filer. */
require_once(ABSPATH . 'wp-settings.php');